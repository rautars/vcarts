Default minecart mod for vanerika subgame
=======================
by PilzAdam, xeranas

Main differences between vcarts and carts mods:

  * vcarts does not include mesecon mod support (no power rails, breaks and etc.)
  * vcarts does not include any rails (reuses default:rail).
  * vcarts works without power rails
  * vcarts:cart recipe slightly different from carts:cart
  * vcarts:cart visually looks different from carts:cart

vcarts mod is compatible with default minetest game, also could be used together with original carts mod.

License of source code:
-----------------------
WTFPL

License of media (textures, sounds and models):
-----------------------------------------------
CC-0

Authors of media files:
-----------------------

xeranas:

  * `vcarts_cart_inventory_side.png`
  * `vcarts_cart_inventory_top.png`
  * `vcarts_cart_uv.png`
  * `vcarts_cart_mesh.obj`

felix.blume (http://freesound.org/people/felix.blume/):

  * `vcarts_cart_sound.ogg` (cut from http://freesound.org/people/felix.blume/sounds/189289/)


